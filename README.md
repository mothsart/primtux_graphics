# Primtux Graphics

L'ensemble des illustrations que j'ai réalisé pour le projet Primtux se trouvent ici.

## rubrique **sources**

On y trouve tous les fichiers (libre de droit) qui m'ont permis de finaliser une illustration : images au format matricielle ou vectoriel, polices de caractères etc.

## rubrique **croquis**

Toute mes participations passent par une phase papier.

Ca me permet d'avoir une première approche de :
- recherche (surtout sur les projets complexes) : brainstorming d'images qui pourraient convenir au projet
- composition : organisation des différents éléments entre eux en leur donnant un sens commun
- de format : portrait ou paysage
- proportions : donner une harmonie des formes en se rappelant les règles du **nombre d'or**.
- perspective : dans les dessins complexes, il est souvent essentiel de construire la ligne d'horizon et les points de fuite
- provenance de la lumière : il est très facile avec le numérique de changer des couleurs de manière général.
Néanmoins, si la provenance de la lumière n'est pas bien déterminer, on se retrouve avec des erreurs difficiles à corriger.
Un premier travail en amont est souvent bénéfique à l'oeuvre définitive.

## rubrique **aperçus**

Le svg n'est pas forcément le format le plus simple à lire : un export en image matricielle est souvent souhaitable. 

## rubrique **réalisations**

Les illustrations définitives.
Les fichiers sont dans le format de travail : çàd non minifié.

Si vous ouvrez un fichier svg sur Inkscape, il aura toutes les méta-informations pour travailler : les différents calques, les guides, les groupes d'objets etc.

